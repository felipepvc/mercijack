module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      sass: {
        dist: {
          options: {                       // Target options
            style: 'compressed',             //  'expanded' or 'compressed'
            lineNumbers: false,
            noCache: true,
            sourcemap: 'none'
          },
          files: [{
            expand: true,
            cwd: 'src/scss/',
            src: ['*.scss'],
            dest: 'public/css/',
            ext: '.min.css'
          }]
        }
      },
      watch: {
        gruntfile: {
          files: 'Gruntfile.js',
          tasks: ['default'],
        },
        css: {
          files: ['src/scss/**'],
          tasks: ['sass', 'postcss', 'svgstore'],
        },
        svgs: {
          files: ['src/svgs/**'],
          tasks: ['svgstore'],
        },
        js: {
          files: ['src/js/**'],
          tasks: ['requirejs'],
        },
      },
      postcss: {
        options: {
          processors: [
            require('autoprefixer')({browsers: 'last 5 versions'}), // add vendor prefixes
            require('cssnano')({ // minify the result
              preset: ['default', {
                  discardComments: {
                      removeAll: false,
                  },
              }]
            })
          ]
        },
        dist: {
          src: 'public/css/*.css'
        }
      },
      browserSync: {
        dev: {
            bsFiles: {
                src : './public/**/*'
            },
            options: {
                watchTask: true,
                server: './public'
            }
        }
      },
      svgstore: {
        options: {
          cleanup: true,
          cleanupdefs: true,
          includeTitleElement: false,
          preserveDescElement: false,
          convertNameToId: function(name) {
              var dotPos = name.indexOf('.');
              if ( dotPos > -1){
                name = name.substring(0, dotPos);
              }
              return name;
            }
        },
        default : {
          files: {
            './public/imgs/svgs/icons.svg': ['./src/svgs/**/*.svg'],
          },
        },
      },
      requirejs: {
        compile: {
          options: {
            baseUrl: './',
            mainConfigFile: 'src/js/config.js',
            name: 'node_modules/almond/almond.js',
            wrap: {
              "startFile": "src/js/wrap.start", //wrapping before deliverable
              "endFile": "src/js/wrap.end" //wrapping after deliverable
            },
            optimize: 'uglify', // none / uglify2
            include: [ 'main' ],
            out: 'public/js/main.js'
          }
        }
      }
    });

    // Load the plugins that provides the tasks.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    // Default task(s).
    grunt.registerTask('default', ['sass', 'postcss', 'requirejs', 'svgstore', 'browserSync', 'watch']);
    grunt.registerTask('build', ['sass', 'postcss', 'requirejs', 'svgstore']);
  };
