define([
    "wowjs",
    "svgxuse",
    "slick",
    "jquery"
], function () {
    'use strict';

    var wow = {};

    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    });
    wow.init();

    // var scene = document.getElementById('parallax')
    // var parallaxInstance = new Parallax(scene);


    // JavaScript
    // var sceneElements = [
    //     document.getElementById('scene-a'),
    //     document.getElementById('scene-b')
    // ]
    // ...or you could do:
    // var sceneElements = document.querySelectorAll('.parallax-items')

    // // ...then loop over the scene elements and create the Parallax instances
    // var parallaxScenes = []
    // for (var i = 0; i < sceneElements.length; i++) {
    //     parallaxScenes.push(new Parallax(sceneElements[i]))
    // }


    // //Intercom integration
    var APP_ID = "lkndmj3k";

    window.intercomSettings = {
        app_id: APP_ID
    };
    var w = window;
    var ic = w.Intercom;
    if (typeof ic === "function") {
        ic("reattach_activator");
        ic("update", intercomSettings);
    } else {
        var d = document;
        var i = function () {
            i.c(arguments);
        };
        i.q = [];
        i.c = function (args) {
            i.q.push(args);
        };
        w.Intercom = i;

        function l() {
            var s = d.createElement("script");
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://widget.intercom.io/widget/" + APP_ID;
            var x = d.getElementsByTagName("script")[0];
            x.parentNode.insertBefore(s, x);
        }
        if (w.attachEvent) {
            w.attachEvent("onload", l);
        } else {
            w.addEventListener("load", l, false);
        }
    }

    return false;
});