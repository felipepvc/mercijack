requirejs.config({
    // baseUrl: 'node_modules/',
    waitSeconds: 20,
    paths: {
        "Parallax": "node_modules/parallax-js/dist/parallax.min",
        "wowjs": "node_modules/wowjs/dist/wow",
        "svgxuse": "node_modules/svgxuse/svgxuse",
        "Vue": "node_modules/vue/dist/vue.min",
        "jquery":"node_modules/jquery/dist/jquery.slim.min",
        "slick":"node_modules/slick-carousel/slick/slick.min",
        "main": "src/js/main"
    }
});