BUCKET=website-prod
gsutil -m rm -r gs://$BUCKET/*
gsutil -m -h "Cache-Control:public,max-age=30"  cp -r -a public-read public/* gs://$BUCKET/
gsutil web set -m index.html -e index.html gs://$BUCKET
