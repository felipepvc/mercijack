const English = {
    header: {
        menu: {
            labelToOpen: "Our solutions",
            labelToClose: "Close"
        },
        phoneNumber: "+351 961 515 610",
        cta: {
            mobile: {
                label: "Login",
                link: "https://app.mercijack.co/"
            },
            desktop: {
                label: "Login",
                link: "https://app.mercijack.co/"
            }
        }
    },
    sidePanel: {
        menu: {
            item1: {
                label: "Our solutions",
                link: "/"
            },
            item2: {
                label: "Become a Jack",
                link: "https://mercijack.typeform.com/to/YiPltM"
            },
            item3: {
                label: "Blog",
                link: "#"
            },
            item4: {
                label: "Merci Jack",
                link: "#"
            },
            "comingSoon": "Bientôt disponible..."
        },
        contacts: {
            title: "Réseaux sociaux",
            facebook: "https://www.facebook.com/mercijack.co/",
            instagram: "https://www.instagram.com/mercijack/",
            twitter: "https://twitter.com/merci_jack",
            linkedin: "https://www.linkedin.com/company/merci-jack/"
        },
        copyright: "All rights reserved. 2018"
    },
    section1: {
        title: "Vos locaux sont entre de bonnes mains",
        subtitle: "Une technologie unique et un réseau de professionnels à votre service",
        tools: {
            plomberie: "Plomberie",
            manutention: "Manutention",
            montage: "Montage",
            bricolage: "Bricolage",
            serrurerie: "Serrurerie",
            electricite: "Électricité"
        }
    },
    section2: {
        title: "<strong class='font-weight-bold'>Plus de 80 clients </strong> gèrent la maintenance de leur locaux et optimisent leur budget avec Merci Jack",
        cta: "Contactez-nous",
        ctaLink: "https://mercijack.typeform.com/to/VZllnj"
    },
    section3: {
        title: "Une plateforme adaptée à tous vos besoins",
        subtitle: "Avec Merci Jack, contrôlez en temps réel la maintenance et les travaux de vos locaux"
    },
    section4: {
        column1: {
            title: "Entretien technique",
            subtitle: "Des artisans certifiés en plomberie, manutention, montage, bricolage, serrurerie et électricité"
        },
        column2: {
            title: "Suivi en temps réel",
            subtitle: "Pilotez, organisez et vérifiez toutes les interventions de vos locaux en un clin d'oeil"
        },
        column3: {
            title: "Analyse de vos besoins",
            subtitle: "Optimisez votre budget en accédant aux rapports et à l'historique de votre maintenance"
        }
    },
    section5: {
        testimony1: {
            author: "Etienne Alessandrini",
            city: "Directeur Technique @BigMamma",
            img: "imgs/solutions/clients/client-big-mama.png",
            title: "",
            body: "L’app et la plateforme sont intuitives, permettent de saisir très rapidement des demandes d’intervention et surtout sont optimisées régulierement en fonction des besoins des clients."
        },
        testimony2: {
            author: "Florian Cochet",
            city: "Morning Manager @MorningCoworking",
            img: "imgs/solutions/clients/client-morning-coworking.png",
            title: "",
            body: "Que ce soit sur l'application mobile ou sur ordinateur, il est toujours simple de suivre plusieurs interventions simultanées, sur des sites différents."
        },
        testimony3: {
            author: "Pierre Lejeune",
            city: "Operations Manager @FoodCheri",
            img: "imgs/solutions/clients/client-food-cheri.png",
            title: "",
            body: "J'utilise l'app Merci Jack super pratique, prise de rendez-vous rapide. Choix des besoins précis."
        }
    },
    section6: {
        title: "Nos artisans à votre service"
    },
    section7: {
        column1: {
            title: "Jack à Tout Faire",
            subtitle: "Bricoleur Professionel",
            body: "Quand on sait que le service de bricolage représente 90% des besoins de nos clients, mieux vaut être bien accompagné !"
        },
        column2: {
            title: "Jack Expert",
            subtitle: "Technicien Spécialisé",
            body: "Plombier, électricien, serrurier... Si vous avez besoin d'une expertise technique, pas de doute, c'est lui qu'il faut appeler."
        },
        column3: {
            title: "Jack Manager",
            subtitle: "Responsable Maintenance",
            body: "L'assistant idéal qui gère tout à votre place : les relevés techniques, le type d'intervention à planifier, le suivi des commandes..."
        }
    },
    section8: {
        title: "Notre Objectif ? Valoriser l'artisan et optimiser son travail.",
        body: "Fini les devis, fini l'administratif, fini la gestion des matériaux.... Merci Jack s’occupe de tout."
    },
    section9: {
        column1: {
            title: "Une école de référencement et de formation en ligne pour apprendre les bases, les compétences et les règles de bonne conduite d'un vrai Jack charmant et professionnel."
        },
        column2: {
            title: "Un service de logistique et de livraison de matériel réservé aux professionnels. Merci Jack se charge de livrer le bon matériel dans l'heure ou sur rendez-vous."
        },
        column3: {
            title: "Une plateforme qui accompagne le Jack au quotidien. Elle permet de planifier les interventions, de suivre les revenus et de consulter les historiques de commande."
        }
    },
    section10: {
        testimony1: {
            author: "Aziza Chaouachi",
            city: "CEO @OuiRent",
            img: "imgs/solutions/clients/client-oui-rent.png",
            title: "",
            body: "(…) Les jacks sont ponctuels, honnêtes, impeccables, professionnels... et comme si ça ne suffisait pas, ils sont souriants et de bonne humeur ! Merci mille fois."
        },
        testimony2: {
            author: "Etienne Alessandrini",
            city: "Directeur Technique @BigMamma",
            img: "imgs/solutions/clients/client-big-mama.png",
            title: "",
            body: "Réactivité au top, peu importe le jour, l’heure ou le type d’intervention demandé il y a toujours un Jack de disponible. Idem pour la team support qui nous accompagne."
        },
        testimony3: {
            author: "Fani Robert",
            city: "Morning Manager @MorningCoworking",
            img: "imgs/solutions/clients/client-morning-coworking.png",
            title: "",
            body: "Toujours sympathiques, les Jack prennent le temps de nous expliquer leurs interventions. Et ils sont ponctuels !"
        }
    },
    section11: {
        title: "Une offre flexible et adaptée à vos besoins",
        subtitle: "Intervention en urgence ou besoins réguliers, petits bureaux ou grands locaux, nous avons une solution pour chaque entreprise."
    },
    section12: {
        expand: "ouvrir",
        column1: {
            title: "Offre Classique",
            line1: "Pour les urgences et les besoins ponctuels",
            line2: "Paiement à la commande",
            line3: "Accès aux analyses data de votre maintenance",
            line4: "Plateforme web & mobile pour suivre vos commandes, consulter votre historiques et contacter le support",
            cta: "Créer son compte",
            ctaLink: "https://mercijack.typeform.com/to/OnjUuH"
        },
        column2: {
            title: "Offre Credits",
            line1: "Des interventions à la carte dans un cadre budgétaire précis",
            line2: "Chargez votre compte et utilisez vos crédits comme vous le souhaitez",
            line3: "Accès aux analyses data de votre maintenance",
            line4: "Plateforme web & mobile pour suivre vos commandes, consulter votre historiques et contacter le support",
            cta: "Demander un devis",
            ctaLink: "https://mercijack.typeform.com/to/OnjUuH",
            economy: "d'économie",
            "creditsDefault": 1000,
            "discountDefault": 70,
            "percentageDefault": 7,
            range: {
                min: 100,
                max: 2000,
                value: 1000,
                step: 50
            }
        },
        column3: {
            title: "Offre Premium",
            line1: "Un accompagnement au quotidien pour vos besoins réguliers",
            line2: "Sans engagement",
            line3: "Prix unique pour tous nos services (consommables inclus)",
            line4: "Account Manager et équipe de Jack attitrée",
            line5: "Jack Management inclus : relevés techniques, logistique et suivi de commande",
            line6: "Accès Deliv Jack pour la livraison de matériel",
            line7: "Facturation unique à la fin du mois",
            line8: "Plateforme web & mobile pour suivre vos commandes, consulter votre historiques et contacter le support",
            cta: "Prendre RDV",
            ctaLink: "https://mercijack.typeform.com/to/OnjUuH"
        }
    },
    section13: {
        testimony1: {
            author: "Etienne Alessandrini",
            city: "Directeur Technique @BigMamma",
            img: "imgs/solutions/clients/client-big-mama.png",
            title: "",
            body: "Merci Jack a su s’adapter à nos besoins et proposer une offre sur mesure qui évolue en fonction de nos besoins."
        },
        testimony2: {
            author: "Tânia Boros",
            city: "Directrice technique @Morning Coworking",
            img: "imgs/solutions/clients/client-morning-coworking.png",
            title: "",
            body: "Le prix unique permet d'éviter les mauvaises suprises, et le service est toujours de qualité !"
        }
    },
    section14: {
        title: "Toujours là ? Si vous voulez en savoir plus, envoyez-nous un message !",
        cta: {
            label: "Contactez-nous",
            link: "https://mercijack.typeform.com/to/VZllnj"
        }
    },
    footer: {
        column1: {
            title: "Qui sommes-nous ?",
            body: "Nous sommes des entrepreneurs qui souhaitent révolutionner la maintenance des locaux professionnels. Grâce à notre propre technologie et à notre réseau d'experts (plombiers, serruriers, électriciens, hommes à tout faire), nous avons voulu créer un service où les clients comme les artisans sont gagnants.",
            cta: "Contactez-nous",
            ctaLink: "https://mercijack.typeform.com/to/VZllnj"
        },
        column2: {
            title: "Rubriques",
            menu: {
                item1: {
                    label: "Nos solutions",
                    link: "/"
                },
                item2: {
                    label: "Devenir un Jack",
                    link: "https://mercijack.typeform.com/to/YiPltM"
                },
                item3: {
                    label: "Blog",
                    link: "#"
                }
            }
        },
        column3: {
            title: "Contact",
            body: "hello@mercijack.co",
            mailto: "mailto:hello@mercijack.co",
            social: {
                title: "Réseaux sociaux",
                facebook: "https://www.facebook.com/mercijack.co/",
                instagram: "https://www.instagram.com/mercijack/",
                twitter: "https://twitter.com/merci_jack",
                linkedin: "https://www.linkedin.com/company/merci-jack/"
            }
        },
        copyright: "Copyright @ 2018 Merci Jack. All rights reserved."
    }
}